# terraform-python

A Docker container that includes Terraform, Python and AWSCLI.  Functions identically to the official Terraform container.

Pre-built containers are available on gitlab image registry: \
E.g. the image URL for TF version 1.1.9 would be `registry.gitlab.com/claranet-pcp/docker/docker-terraform-python:1.1.9`

## Versioning
Container versions are as follows:

```
<registry>:<tf-version>
```

So for example, `0.12.3` would be the container with Terraform version 0.12.3.

If you build multiple images of the same Terraform version, the pipeline will automatically increment a number to the image that is created, for example `0.12.3-1`. 

## Building the Image.
The image is hosted in the gitlab container registry and is built by Gitlab CI

To add a new version to the automatic build pipeline add the version to the matrix in the build_image step.

``` yaml
build_image:
  stage: build
  tags:
    - gitlab-org-docker
  dependencies: []
  parallel:
    matrix:
      - TF_VERS: [1.9.4, 1.1.9, 1.1.8, 0.12.3]
```

Should you need to manually build a container use the below steps

```
# Build the container
$ docker build . -t registry.gitlab.com/claranet-pcp/docker/docker-terraform-python:<tf-version>

# Push to Registry
$ docker push registry.gitlab.com/claranet-pcp/docker/docker-terraform-python:<tf-version>

# Update 'latest' tag
$ docker tag 
```
